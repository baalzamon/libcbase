#!/usr/bin/env sh
# Run this after changing debug CFLAGS or adding a new source file.
# It generates a file which configures clangd to use the debug build CFLAGS.

which bear >/dev/null 2>&1 </dev/null || {
	printf 'Bear is needed to generate compile commands for clangd from the Makefile.\n' 1>&2
	printf 'This is only necessary for those who use clangd and want it to be accurate.\n' 1>&2
	printf 'Install bear. (Check here: <https://github.com/rizsotto/Bear>.)\n' 1>&2
	exit 1
}

make clean
bear -- make -r "-j$(($(nproc) + 1))" debug
exec make clean
