#!/usr/bin/env sh
# For ease of typing.

exec make -r "-j$(($(nproc --all) + 1))" "${@}"
