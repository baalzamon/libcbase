PROJECT:=libcbase_test

CC:=clang

BUILD_DIR:=./build
SRC_DIRS:=./src ./include

# Find all files needed for compilation.
SRCS:=$(shell find -O3 $(SRC_DIRS) -type f -a -name '*.c')
ANALYSE_SRCS:=$(shell find -O3 $(SRC_DIRS) -type f -a -name '*.[ch]')

# Turn them all into object files.
OBJS:=$(SRCS:%=$(BUILD_DIR)/%.o)

# And now for dependencies.
DEPS:=$(SRCS:%=$(BUILD_DIR)/%.d)

# Directories which contain header files.
INC_DIRS:=./src ./include
INC_FLAGS:=$(addprefix -I,$(INC_DIRS))

# Libraries against which to link.
LIBS:=libpipewire-0.3
LDFILES:=$(shell pkg-config --static --libs $(LIBS) 2>/dev/null) -lm
LDFLAGS:=$(shell pkg-config --cflags $(LIBS) 2>/dev/null)


WARNS:=-Wall -Wextra -Wshadow -Wunreachable-code -Wconversion -Wsign-conversion -Wformat -Wmissing-braces -Wparentheses -Wreserved-identifier -pedantic \
-Wno-unused-command-line-argument -Wno-incompatible-pointer-types-discards-qualifiers -Wno-extra-semi

CPPFLAGS_DEPS:=-MMD -MP
CPPFLAGS_COMMON:=$(WARNS) $(INC_FLAGS) $(CPPFLAGS_DEPS) $(LDFLAGS) -D_DEFAULT_SOURCE=1 -D_FILE_OFFSET_BITS=64 -D_GNU_SOURCE=1
CPPFLAGS_DEBUG:=$(CPPFLAGS_COMMON) -DDEBUG=1 -DCB_DEBUG=1
CPPFLAGS_RELEASE:=$(CPPFLAGS_COMMON) -DNDEBUG=1 -DCB_LOG_LEVEL_COMPILE_TIME_MIN=CB_LOG_LEVEL_INFO

CFLAGS_SANITIZER:=-fsanitize=address,undefined -fno-sanitize-trap=all -fsanitize-address-use-after-return=always -fsanitize-address-use-after-scope -fno-omit-frame-pointer -fno-optimize-sibling-calls

CFLAGS_COMMON:=-std=c23 -pipe -fwrapv -fpie -fno-plt
CFLAGS_DEBUG:=$(CPPFLAGS_DEBUG) $(CFLAGS_COMMON) $(CFLAGS_SANITIZER) -ggdb3 -gdwarf64 -rdynamic -O0 -ftrapv
CFLAGS_RELEASE:=$(CPPFLAGS_RELEASE) $(CFLAGS_COMMON) -march=native -mtune=native -g0 -s -O3 -fomit-frame-pointer -flto=full


COMPILE_MODE:=debug
CFLAGS:=$(CFLAGS_DEBUG)

.PHONY: debug
debug: CFLAGS:=$(CFLAGS_DEBUG)
debug: $(BUILD_DIR)/$(PROJECT)

.PHONY: release
release: CFLAGS:=$(CFLAGS_RELEASE)
release: $(BUILD_DIR)/$(PROJECT)


$(BUILD_DIR)/$(PROJECT): $(OBJS)
	@-printf 'LD\t%s\n' "$@"
	@mkdir -p $(dir $@)
	@"$(CC)" $(CFLAGS) $(LDFILES) -fuse-ld=lld -pie $^ -o "$@"

$(BUILD_DIR)/%.c.o: %.c
	@-printf 'CC\t%s\n' "$@"
	@mkdir -p $(dir $@)
	@"$(CC)" $(CFLAGS) -c "$<" -o "$@"


.PHONY: analyse
analyse:
	-clang-tidy $(ANALYSE_SRCS) -- $(CFLAGS_DEBUG)

.PHONY: clean
clean:
	rm -rf "$(BUILD_DIR)"

-include $(DEPS)
