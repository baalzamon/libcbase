#include <stdio.h>
#include <stdlib.h>
#include <threads.h>

#include <libcbase/common.h>
#include <libcbase/vec.h>
#include <libcbase/list.h>
#define LIBCBASE_LOG_IMPLEMENTATION
#include <libcbase/log.h>
#define LIBCBASE_ARENA_IMPLEMENTATION
#include <libcbase/arena.h>
#define LIBCBASE_FS_IMPLEMENTATION
#include <libcbase/fs.h>


#define CB_LOG_MODULE "test"


cb_vec(my_vec, uint);


static void my_vec_print(my_vec vec[static 1]);


int main(void) {
	fclose(stdin);
	cb_log_init(stderr, CB_LOG_LEVEL_TRACE, CB_LOG_FEATURE_COLOUR_INFER);

	LOG_DEBUG("Hello, world!");
	LOGE_DEBUG(2, "Hello, world!");
	LOGM_DEBUG("Hello, world!");
	LOGEM_DEBUG(2, "Hello, world!");

	LOGM_WARN("Sleeping for 1.01 seconds.");
	thrd_sleep(&(struct timespec){.tv_sec = 1L, .tv_nsec = 10000000L}, 0);

	my_vec *vec = my_vec_create(0);
	if (!vec)
		return EXIT_FAILURE;

#define VEC_TEST_SIZE 10U
	for (uint i = 0; i < VEC_TEST_SIZE; ++i) {
		my_vec_push(vec, &i);
		printf("Iteration %u (size: %-2zu, cap: %-2zu):\n", i, vec->size, vec->cap);
		my_vec_print(vec);
	}

	// Check push works.
	for (uint i = VEC_TEST_SIZE; i > 0; --i) {
		if (vec->p[VEC_TEST_SIZE - i] != i - 1) {
			LOG_FATAL("Uh-oh!");
			my_vec_destroy(vec);
			return EXIT_FAILURE;
		}
	}

	LOG_DEBUG("vec->cap == %zu", vec->cap);
	my_vec_destroy(vec);

	cb_arena *arena = cb_arena_new_zeroed(malloc(sizeof(cb_arena)), KiB(1));
	if (!arena)
		return EXIT_FAILURE;
	if (!arena->start) {
		free(arena);
		return EXIT_FAILURE;
	}

	char *my_str = cb_arena_alloc(arena, 128);
	if (!my_str) { // Logically, this could never happen.
		LOG_FATAL("Library error!");
		cb_arena_destroy(arena);
		free(arena);
		return EXIT_FAILURE;
	}
	strlcpy(my_str, "Hello, world! This is my arena-allocated string!", 128);
	LOG_INFO("%.128s", my_str);

	char *my_str2 = cb_arena_alloc(arena, 128);
	if (!my_str2) {
		LOG_FATAL("Library error!");
		cb_arena_destroy(arena);
		free(arena);
		return EXIT_FAILURE;
	}

	strlcpy(my_str2, "Hello, world! This is my second arena-allocated string!", 128);
	LOG_INFO("%.128s", my_str2);

	cb_arena_destroy(arena);
	free(arena);
	return EXIT_SUCCESS;
}


static void my_vec_print(my_vec vec[static 1]) {
	if (!vec->size)
		return;

	flockfile(stdout);
	printf("%u", vec->p[0]);
	for (size_t i = 1; i < vec->size; ++i)
		printf(", %u", vec->p[i]);
	puts("");
	funlockfile(stdout);
}
