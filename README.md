# libcbase

### About

My collection of useful functions et al., including (so far):

- Rudimentary `stderr` logging (which looks pretty good)
- Type-generic doubly-linked list (not completed)
- Type-generic vector/dynamic array (similar to `std::vector` but better because C)
- Functions for writing and reading files

**Written with features from C11 and C17 and maybe C23.**

## Usage

### Build

The flags will likely only work with clang, so edit the Makefile to make it work with your compiler, if necessary.

Compile with

```sh
$ ./remake.sh
```

### Using

Include the wanted header and its dependencies in your project, and read the header preamble.

Read [main.c](src/main.c) and [the headers](include/libcbase) to understand.

### Dependencies

- **GNU/Linux** (as it is now, it relies on <unistd.h>)
- **make** (for building via the Makefile)
- **clang** (or GCC or any other C17 compiler)

## License

Licensed under the [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html). See [LICENSE.md](./LICENSE.md).
