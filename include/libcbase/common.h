#ifndef LIBCBASE_COMMON_H_
#define LIBCBASE_COMMON_H_

#include <stdint.h>


#define CB_UNUSED(var) (void)(var)

// ONLY USE THESE WITH CONSTANTS, MATHS, OR VARIABLE ACCESSES.
#define CB_MAX(a, b) ((a) > (b) ? (a) : (b))
#define CB_MIN(a, b) ((a) < (b) ? (a) : (b))


typedef int8_t  s8;
typedef uint8_t u8;

typedef int16_t  s16;
typedef uint16_t u16;

typedef int32_t  s32;
typedef uint32_t u32;

typedef int64_t  s64;
typedef uint64_t u64;

typedef unsigned char      uchar;
typedef unsigned short     ushort;
typedef unsigned int       uint;
typedef unsigned long      ulong;
typedef unsigned long long ullong;

typedef long long llong;


// Constants for byte sizes.
#define KB_c ((size_t)1000)
#define MB_c ((size_t)1000 * KB_c)
#define GB_c ((size_t)1000 * MB_c)
#define TB_c ((size_t)1000 * GB_c)

// Prefer *iB, as they're powers of two.
#define KiB_c ((size_t)1024)
#define MiB_c ((size_t)1024 * KiB_c)
#define GiB_c ((size_t)1024 * MiB_c)
#define TiB_c ((size_t)1024 * GiB_c)

// Simplifies operator precedence over using the above.
#define KB(v) ((size_t)(v) * KB_c)
#define MB(v) ((size_t)(v) * MB_c)
#define GB(v) ((size_t)(v) * GB_c)
#define TB(v) ((size_t)(v) * TB_c)

#define KiB(v) ((size_t)(v) * KiB_c)
#define MiB(v) ((size_t)(v) * MiB_c)
#define GiB(v) ((size_t)(v) * GiB_c)
#define TiB(v) ((size_t)(v) * TiB_c)

#endif /* LIBCBASE_COMMON_H_ */
